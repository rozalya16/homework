// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив,
//     который будет содержать в себе любые данные, второй аргумент - тип данных.
//     Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент,
//     за исключением тех, тип которых был передан вторым аргументом. То есть, если передать
// массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет
// массив [23, null].

var arr = ['hello', 'world', 23, '23', null];
const filterBy = function (arr,type){
    return arr.filter(x=> typeof(x) !== type);
};
console.log(filterBy(arr,'object')); // not null
console.log(filterBy(arr,'number')); // not 23
console.log(filterBy(arr,'string')); // not 'hello', 'world','23'


