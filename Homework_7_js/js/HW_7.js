// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент
// parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением
// его на страницу;
"use strict";

const newArray = [
    "hello",
    "world",
    "Kiev",
    "Kharkiv",
    "Odessa",
    "Lviv",
    "1",
    "2",
    "3",
    "sea",
    "user",
    23,
];

const renderList = (arr, dbody = document.body) => {
    let li = arr.map((word) => `<li>${word}</li>`);
    let clearLi = li.join("");
    let ul = document.createElement("ul");
    ul.innerHTML = clearLi;
    dbody.prepend(ul);
};

renderList(newArray);