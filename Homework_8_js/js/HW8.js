"use strict";

let divspan = document.createElement("div");
document.body.append(divspan);

let div = document.createElement("div");
document.body.append(div);
div.classList.add("form");

let input = document.createElement("input");
input.placeholder = "Price";
div.append(input);

input.addEventListener("focus", function (e)
{
    e.target.style.border = "10px solid green";
});

let currPrice = document.createElement("span");
divspan.append(currPrice);

let closeBtn = document.createElement("button");
closeBtn.innerText = "X";

input.addEventListener("blur", function (e)
{
    e.target.style.border = "2px solid lightgrey";
    currPrice.innerHTML = input.value;

    if (input.value < 0 || input.value === "") {
        e.target.style.color = "#000000";
        e.target.style.border = "1px solid red";
        currPrice.remove();
        e.target.insertAdjacentHTML("afterEnd", "<br>Please enter correct price");
    }
    else
        {
        e.target.style.color = "#00FF00";
        currPrice.innerHTML = "Текущая цена: " + input.value;
        divspan.append(currPrice);
        divspan.append(closeBtn);
    }
});

closeBtn.addEventListener("click", function (e) {
    input.value = "";
    closeBtn.remove();
    currPrice.remove();
});