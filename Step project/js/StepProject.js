"use strict";

// Our Services

let tabtitle = document.querySelectorAll(".services__tabs-item");
let content = document.querySelectorAll(".tabs__content");
let target;

tabtitle.forEach(function (tabclick) {
    tabclick.addEventListener("click", (e) => {
        tabtitle.forEach((tabclick) => {
            tabclick.classList.remove("active");
        });
        tabclick.classList.add("active");
        target = tabclick.getAttribute("data-target");
        changecontent(target);
    });
});

function changecontent(target) {
    content.forEach((e) => {
        if (e.classList.contains(target)) {
            e.classList.add("active");
        } else e.classList.remove("active");
    });
}

// Amazing

let amTitle = document.querySelectorAll(".amazing__tabs-item");
let amContent = document.querySelectorAll(".amazing__img");
let amTarget;
let loadMore = document.querySelector(".amazing__load");

loadMore.addEventListener("click", () => {
    loadMore.hidden = true;
    let target = document
        .querySelector(".amazing__tabs-item.active")
        .getAttribute("data-target");
    amContent.forEach((e) => {
        if (target == "target-all") {
            e.classList.add("active");
        } else {
            if (e.classList.contains(target)) {
                e.classList.add("active");
            } else e.classList.remove("active");
        }
    });
});

amTitle.forEach(function (amclick) {
    amclick.addEventListener("click", (e) => {
        loadMore.hidden = false;
        amTitle.forEach((amclick) => {
            amclick.classList.remove("active");
        });
        amclick.classList.add("active");
        amTarget = amclick.getAttribute("data-target");
        changeAmContent(amTarget);
    });
});

function changeAmContent(amTarget) {
    let counter = 0;
    amContent.forEach((e) => {
        if (amTarget == "target-all") {
            if (counter < 12) {
                e.classList.add("active");
            } else e.classList.remove("active");
            counter++;
        } else {
            if (e.classList.contains(amTarget)) {
                if (counter < 4) {
                    e.classList.add("active");
                } else e.classList.remove("active");
                counter++;
            } else e.classList.remove("active");
        }
    });
}

// What People Say

let aboutTitle = document.querySelectorAll(".about__tabs-item");
let aboutContent = document.querySelectorAll(".about__content-item");
let aboutTarget;

aboutTitle.forEach(function (aboutclick) {
    aboutclick.addEventListener("click", (e) => {
        aboutTitle.forEach((aboutclick) => {
            aboutclick.classList.remove("active");
        });
        aboutclick.classList.add("active");
        aboutTarget = aboutclick.getAttribute("data-target");
        currentSlide = aboutTarget.replace("target-", "");
        currentSlide--;
        changeAboutContent(aboutTarget);
    });
});

function changeAboutContent(aboutTarget) {
    aboutContent.forEach((e) => {
        if (e.classList.contains(aboutTarget)) {
            e.classList.add("active");
        } else e.classList.remove("active");
    });
}

// Prev & Next buttons
const slides = document.querySelectorAll(".about__tabs .about__tabs-item");
let currentSlide = 0;
const btnBack = document.getElementById("about__back");
btnBack.addEventListener("click", back);
const btnNext = document.getElementById("about__next");
btnNext.addEventListener("click", next);

function back() {
    slides[currentSlide].className = "about__tabs-item";
    if (currentSlide == 0) {
        currentSlide = 3;
        slides[currentSlide].className = "about__tabs-item active";
        aboutTarget = slides[currentSlide].getAttribute("data-target");
        changeAboutContent(aboutTarget);
    } else {
        currentSlide = (currentSlide - 1) % slides.length;
        slides[currentSlide].className = "about__tabs-item active";
        aboutTarget = slides[currentSlide].getAttribute("data-target");
        changeAboutContent(aboutTarget);
    }
}

function next() {
    aboutTitle.forEach((aboutclick) => {
        aboutclick.classList.remove("active");
    });
    if (currentSlide == 4) {
        currentSlide = 0;
        slides[currentSlide].className = "about__tabs-item active";
        aboutTarget = slides[currentSlide].getAttribute("data-target");

        changeAboutContent(aboutTarget);
    } else {
        currentSlide++;
        if (currentSlide == 4) {
            currentSlide = 0;
        }
        slides[currentSlide].className = "about__tabs-item active";
        aboutTarget = slides[currentSlide].getAttribute("data-target");

        changeAboutContent(aboutTarget);
    }
}
