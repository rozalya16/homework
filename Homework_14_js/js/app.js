"use strict";

$(".menu").on("click", "a", function (e) {
  e.preventDefault();
  const id = $(this).attr("data-target");
  const top = $(id).offset().top;
  $("body, html").animate(
    {
      scrollTop: top,
    },
    3000
  );
});

$(function () {
  $(".up-button").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      1000
    );
  });
});

$(window).scroll(function () {
  if ($(this).scrollTop() >= screen.height) {
    $(".up-button").fadeIn();
  } else {
    $(".up-button").fadeOut();
  }
});

$(".toggle").on("click", function (e) {
  e.preventDefault();
  $(".hot-news").slideToggle("slow");
});
