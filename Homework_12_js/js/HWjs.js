"use strict";

const images = Array.from(document.getElementsByClassName("image-to-show"));
const stopAnimBtn = document.createElement("button");
stopAnimBtn.id = "stop-anim-btn";
stopAnimBtn.textContent = "Прекратить";

const goOnBtn = document.createElement("button");
goOnBtn.id = "go-on-btn";
goOnBtn.style.display = "none";
goOnBtn.textContent = "Возобновить показ";

stopAnimBtn.addEventListener("click", function () {
    clearTimeout(animation);
    stopAnimBtn.style.display = "none";
    goOnBtn.style.display = "block";
});

goOnBtn.addEventListener("click", function () {
    animation = setInterval(changePhoto, 3000);
    stopAnimBtn.style.display = "block";
    goOnBtn.style.display = "none";
});

images.forEach((el) => { el.style.display = "none";});
images[0].style.display = "block";

let animation = setInterval(changePhoto, 3000);

let i = 1;
function changePhoto() {
    images.forEach((el) => { el.style.display = "none"; });

    images[i].style.display = "block";
    i++;
    if (i >= images.length) { i = 0;}
}

document.getElementsByClassName("images-wrapper")[0].after(goOnBtn);
document.getElementsByClassName("images-wrapper")[0].after(stopAnimBtn);
